[![pipeline status](https://gitlab.com/tdameritrade-tools/tdameritrade-cli/badges/master/pipeline.svg)](https://gitlab.com/tdameritrade-tools/tdameritrade-cli/commits/master) [![Documentation Status](https://readthedocs.org/projects/tdameritrade-cli/badge/?version=latest)](https://tdameritrade-cli.readthedocs.io/en/latest/?badge=latest) [![coverage report](https://gitlab.com/tdameritrade-tools/tdameritrade-cli/badges/master/coverage.svg)](https://gitlab.com/tdameritrade-tools/tdameritrade-cli/commits/master)




# TDAmeritrade CLI
A CLI that makes requests of TDA using the TDA API via the TDA client. 

Read the [docs](https://tdameritrade-cli.readthedocs.io/en/latest/?#).

## Installation:
Follow the instructions [here](https://tdameritrade-cli.readthedocs.io/en/latest/installation.html) to install the 
package. 

## Usage:
The CLI has full help text accessible via `tda-cli --help`. For usage examples, consult the 
[quickstart guide](https://tdameritrade-cli.readthedocs.io/en/latest/quickstart.html).
