from collections import OrderedDict

import pytest

from tdameritrade_cli.tools.ods_writer import ODSWriter


class TestODSWriter(object):
    def test_sheet_exists(self):
        writer = ODSWriter('not_a_path')
        assert writer._sheet_exists is False

    def test_write_raises_on_version(self, tmpdir):
        p = tmpdir.join('bad.ods')
        writer = ODSWriter(str(p))
        template = writer._sheet_template
        template['Cover Sheet'][0][-1] = '0.0.0'
        writer._write_to_sheet(template)

        with pytest.raises(AssertionError):
            writer._write_to_sheet(OrderedDict())

    def test__update_metrics(self, tmpdir):
        writer = ODSWriter('')
        pos_sheet = [
            ['ABC', '', 'type:', 'EQUITY'],
            [' '],
            ['01/01/19', 100.0],
            ['01/01/19', 120.0],
            ['01/01/19', 150.0],
        ]
        pos_sheet = writer._update_metrics(pos_sheet)
        expected = (150.0 - 100.0) / 100.0
        assert expected == pos_sheet[2][4]
