import click
import pytest

from tdameritrade_cli.tools.client_factory import ClientFactory


class TestClientFactory(object):
    def test_raises_on_bad_config(self):
        with pytest.raises(click.Abort):
            _ = ClientFactory()

    def test_raises_on_bad_path(self, tmpdir):
        p = tmpdir.mkdir('subdir')
        client_factory = ClientFactory(json_config=p)
        with pytest.raises(IsADirectoryError):
            client_factory.from_json()

    def test_raises_on_bad_json(self, tmpdir):
        p = tmpdir.join('out.json')
        p.write('non json content')
        client_factory = ClientFactory(json_config=p)
        with pytest.raises(ValueError):
            client_factory.from_json()
