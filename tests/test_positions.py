from unittest.mock import patch

import pytest
from tdameritrade_client.client import TDClient

from tdameritrade_cli.tools.positions import get_positions


@patch('tdameritrade_client.client.TDClient.get_positions')
def test_get_positions_handles_error(mocked_get_positions):
    mocked_get_positions.return_value = {
        'error': 'Something went wrong.'
    }
    client = TDClient(acct_number=123, oauth_user_id='abc', redirect_uri='123:456')
    with pytest.raises(SystemExit):
        _ = get_positions(client)


@patch('tdameritrade_client.client.TDClient.get_positions')
def test_get_positions_raises(mocked_get_positions):
    mocked_get_positions.return_value = {
        'unexpected_key': 'value'
    }
    client = TDClient(acct_number=123, oauth_user_id='abc', redirect_uri='123:456')
    with pytest.raises(KeyError):
        _ = get_positions(client)


@patch('tdameritrade_client.client.TDClient.get_positions')
def test_get_positions_returns_correctly(mocked_get_positions):
    mocked_get_positions.return_value = {
        'securitiesAccount': {
            'positions': [{
                'instrument': {
                    'symbol': 'abc',
                    'assetType': 'EQUITY'
                },
                'marketValue': '456',
            }],
            'currentBalances': {'liquidationValue': '1000'}
        }
    }
    client = TDClient(acct_number=123, oauth_user_id='abc', redirect_uri='123:456')
    liq_value, data = get_positions(client)
    assert liq_value == '1000'
    assert data[0] == ['abc', 'EQUITY', '456']

