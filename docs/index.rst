.. tdameritrade-cli documentation master file, created by
   sphinx-quickstart on Fri Mar 29 19:30:57 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to tdameritrade-cli's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   quickstart
   source/modules_entry


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
