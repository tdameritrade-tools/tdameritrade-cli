Quickstart
==========

Before You Begin:
-----------------
This CLI is build on the TDAmeritrade Client PyPi package_.

The package assumes you or someone who trusts you has set up a TDAmeritrade app through their developer
portal. If you haven't done this, see TDAmeritrade's getting started guide_.

To authenticate against the TDAmeritrade API, you need:

1. An OAuth redirect URI
2. An OAuth user ID
3. Your TDAmeritrade account number

You create these first two items when you register a new app with TDAmeritrade.

Ways to Authenticate
====================
You can authenticate by either passing in the three items above individually (with the ``-a``, ``-o``, and ``-r``
options), or by writing a json object
containing the same information::

    {
        "acct_number": 1234567890,
        "oauth_user_id": MYOAUTHID,
        "redirect_uri": http://127.0.0.1:8080
    }


Then, you can point the CLI to this location of this file with the ``-j`` option.

Execute ``tda-cli --help`` for full help text.

Current Uses:
-------------
The CLI has the following uses:

1. Print your positions data to the console::

    tda-cli -j PATH_TO_JSON list-positions

2. Write your positions data to an ods file::

    tda-cli -j PATH_TO_JSON write-positions PATH_TO_ODS_OUTPUT

When writing to an ods, the CLI will generate a cover sheet with current positions, and update current liquidation
throughout the calendar year. Subsequent sheets are time data of each position which is appended every time the command
is run against the same sheet. This functionality is meant to be run automatically and frequently to generate a local
aggregate of your investment data.





.. _package: https://gitlab.com/tdameritrade-tools/tdameritrade-client
.. _guide: https://developer.tdameritrade.com/content/getting-started
