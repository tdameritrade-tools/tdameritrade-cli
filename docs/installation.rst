Installation Instructions
=========================

System Dependencies
-------------------
Python 3.6 or 3.7.

Linux
-----
Create a virtual environment and install with::

    pip install tdameritrade-cli

If your virtual environment is located at ``$HOME/.virtualenvs/NAME_OF_VENV``, then add the CLI to your path with::

    ln -s $HOME/.virtualenvs/NAME_OF_VENV/bin/tda-cli $HOME/.local/bin/tda-cli

And open a new shell. You can test the installation with ``tda-cli --help``

To update the package, navigate to the virtual environment directory and run::

    bin/pip install --upgrade tdameritrade-cli

Windows
-------
1. If not already present on your system, install Python 3 here_.
2. Open a ``CMD`` shell, create a new directory where you'd like to install the CLI, enter it, and create a virtual environment::

    mkdir td-cli
    cd td-cli
    py -m venv .

3. Install the CLI into the virtual environment::

    Scripts\pip install tdameritrade-cli

4. Add the package's Scripts directory (located at ``tda-cli\Scripts``) to your path (instructions_).

5. Open a new shell and test the installation with ``tda-cli --help``.

6. To update the package, navigate to the virtual environment directory and run::

    Scripts\pip install --upgrade tdameritrade-cli

Usage
-----
For usage instructions, consult the quickstart_.

.. _here: https://www.python.org/downloads/release/python-373/
.. _instructions: https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/
.. _quickstart: https://tdameritrade-cli.readthedocs.io/en/latest/quickstart.html

